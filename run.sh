#!/bin/bash -eu

python3 dc-p0t/main.py
docker image build -t security_anthem/victim_con:1 .
# docker run -it -d -p 8080:80 --name victim -v $(pwd)/logs:/usr/local/apache2/logs security_anthem/victim_con:1
docker run -it -d -p 8080:80 --name victim security_anthem/victim_con:1
docker exec victim hostname -i
rm -f tcpdump.pcap
tcpdump -i docker0 -s 0 -w tcpdump.pcap &
./monitor_main.py > ps_monitor.txt &
