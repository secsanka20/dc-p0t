普段の編集作業のやり方
====================
GitLabでの作業の方法を説明します．
初期設定は，下に書いてます．

1. 作業開始前にみんなの作業結果をダウンロード
```
git fetch upstream
git merge upstream/master
```
なんかエラーが出たらファイルを開いておかしい部分を正しく直してください

エラーの原因は，自分以外の人が自分と同じファイルを編集してしまったことであることが多いです．このとき，そのファイルには，2人の編集内容を並べて保存されています．どっちを採用するか決めて自分で編集していらないほうを消してください．

2. プログラムを書くといった作業する．
3. 編集が終わったら，作業結果をフォークした自分のページに反映させる
```
git add .
git commit -m "みんなに分かるように作業内容を書く"
git push origin master
```
4. マージリクエストする
5. マージする

# 初めて使うとき
1. 最初にforkして、自分のアカウントのプロジェクトを作る。
2. `git clone (cloneするURL)`
3. `git remote add upstream git@gitlab.com:security_anthem_2020/dc-p0t.git`

または

3. `git remote add upstream https://gitlab.com/security_anthem_2020/dc-p0t.git`

