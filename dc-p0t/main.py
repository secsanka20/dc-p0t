import json
import generate_dockerfile

with open('init.json', 'r') as json_file:
    init_data = json.load(json_file)

app = init_data.get("app")
version = init_data.get("app_version")
if(app==None or version==None):
    import pprint
    pprint.pprint(app)
    pprint.pprint(version)
    print("Please revise init.json")
    exit(1)
else:
    return_code = generate_dockerfile.write_dockerfile(app,version)
    exit(return_code)
