#!/bin/bash
rm -rf docker_log/*
docker diff victim | sed 's/^C //g' | sed 's/^A //g' | while read line
do
TOTAL=$(docker exec victim ls -l $line | head -n 1 | cut -c 1-5)
if test "$TOTAL" != "total"
then
mkdir -p docker_log/$(dirname $line)
docker cp victim:$line docker_log/$line
fi
done
docker diff victim > docker_diff_result.txt
docker logs victim > docker_logs_result.txt
