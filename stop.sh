#!/bin/bash
ps aux | grep tcpdump | grep -v grep | awk '{ print "kill -INT", $2 }' | sh
./dc-p0t/get_log.sh
docker stop victim
ps aux | grep monitor_main | grep -v grep | awk '{ print "kill -INT", $2 }' | sh
docker rm victim
